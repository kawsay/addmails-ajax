class MailsController < ApplicationController
  
  def index
    @mails = Mail.all.order(id: :desc)
  end

  def create
    @mail = Mail.new(
      object: ["(づ ￣ ³￣)づ",
               "( ͡° ͜ʖ ͡°)",
               "(´ε｀ )",
               "╭∩╮（︶︿︶）╭∩╮ ",
               "(/◕ヮ◕)/",
               "(^o^)",
               "(✿◠‿◠)",
               "o(_ _)o",
               "@( * O * )@",
               "ᕕ( ᐛ )ᕗ ",
               "ლ(ಠ益ಠ)ლ",
               "(☞ﾟ∀ﾟ)☞",
               "(>ლ) "].sample,


      body:   Faker::HitchhikersGuideToTheGalaxy.quote,
      read:   false
    )
    if @mail.save
      respond_to do |format|
        format.html { redirect_to root_path 
                      flash[:success] = "Email created" }
        format.js   { }
      end
    else
      redirect_to root_path
      flash[:alert] = "Beeboop error"
    end
  end

  def show
    @mail = Mail.find(params[:id])

    read(@mail) 

    respond_to do |format|
      format.html { }
      format.js   { }
    end
  end

  def destroy
    @mail = Mail.find(params[:id])

    @mail.destroy
    
    respond_to do |format|
      format.html { redirect_to root_path
                    flash[:success] = "Beeboop supprimé" }
      format.js   { }
    end
  end

  def read(mail)
    mail.update(
      read: true
    )
  end

  def unread
    @mail = Mail.find(params[:id])
    @mail.update(
      read: false
    )
    respond_to do |format|
      format.html { redirect_to root_path
                    flash[:success] = "Beeboop unread" }
      format.js { }
    end
  end

  def goodbye
    @mails = Mail.all
    @mails.delete_all
    respond_to do |format|
      format.html { redirect_to root_path
                    flash[:alert] = "Beeboop pludami" }
      format.js { }
    end
  end
end
