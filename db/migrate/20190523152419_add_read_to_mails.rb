class AddReadToMails < ActiveRecord::Migration[5.2]
  def change
    add_column :mails, :read, :boolean
  end
end
