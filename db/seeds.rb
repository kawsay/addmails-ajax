# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# require 'faker' 


#   mail = Mail.create!(
#     object: Faker::FunnyName.name,
#     body:   Faker::HitchhikersGuideToTheGalaxy.quote,
#     read:   false
#   )
# end
6.times do
print "Doing nothing ...  "
sleep(1)
print "✓\n"
end

40.times do
  print "Waiting for ^C "
  5.times do
    sleep(1)
    print "."
  end
  print "\n"
end


# print "Creating fake tasks...  "
# 3.times do
#   my_category = Category.create(title: Faker::Book.genre)
#   3.times do
#     my_task = Task.new(title:   Faker::Book.title,
#                       deadline: Faker::Date.forward(23),
#                       image:    Faker::Avatar.image)
#     my_task.category = my_category
#     my_task.save
#   end
# end
# print "✓\n"