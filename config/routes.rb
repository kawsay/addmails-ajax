Rails.application.routes.draw do
  devise_for :users
  root to: "mails#index"
  post "/mail/unread/:id", to: "mails#unread", as: 'unread'
  post "/mail/goodbye/", to: "mails#goodbye", as: 'goodbye'
  resources :mails, only: [:index, :create, :show, :destroy]
  resources :tasks, except: [:show]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
